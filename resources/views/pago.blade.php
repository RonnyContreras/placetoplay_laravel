<!DOCTYPE html>
<html>
<head>
	<title>PlacetToPlay</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

		<div class="container">
			<br>
		<h2>Formulario</h2>

		<br><br><hr><br><br>
		<form action="{{ route('pago.store')}}" method="POST">
			{{ csrf_field() }}
			<div form-group>
			    <label for="referencia">Referencia</label>
			    <input type="" name="referencia" required>
			</div>
			<div form-group>
			    <label> Description</label>
			    <input type="" name="descripcion" required>
			</div>
			<div form-group>
			    <label> Moneda</label>
			    <input type="" name="moneda" value="COP" required>
			</div>
			<div form-group>
			    <label> Monto a Pagar</label>
			    <input type="" name="monto" required>
			</div>
			    <button type="submit"> Enviar</button>
			


		</form>
		<br><br>
		<hr>


		<h3>Cache</h3>
		<div class="table-response">
			<table class="table table-bordered" style="width:50%">
			  <tr>
			    <th>requestId</th>
			    <th>Estado</th> 
			    <th>Mensaje</th>
			    <th>Fecha</th>
			  </tr>
			  <tr>
			    <td>{{ Cache::get('requestId') }}</td>
			    <td>{{ Cache::get('status') }}</td> 
			    <td>{{ Cache::get('message') }}</td>
			     <td>{{ Cache::get('date') }}</td>
			  </tr>

			</table>
		</div>

		<h3>Registros</h3>
		<div class="table-response">
			<table class="table table-bordered" style="width:50%">
			  <tr>
			    <th>requestId</th>
			    <th>Estado</th> 
			    <th>Mensaje</th>
			    <th>Fecha</th>
			  </tr>
			    <?php foreach ($registros as $registro): ?>
			    	<tr>
				    	<td>{{ $registro->requestId }}</td>
					    <td>{{ $registro->status }}</td> 
					    <td>{{ $registro->message }}</td>
					    <td>{{ $registro->date }}</td>
				    </tr>
			    <?php endforeach ?>
			    
			</table>
		</div>
	</div>

</body>
</html>


