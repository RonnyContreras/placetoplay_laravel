<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dnetix\Redirection\PlacetoPay;
use App\Registro;
use Session;
use Cache;

class pagoController extends Controller
{
    public function index(){
    	
    	$registros = Registro::All();

    	return view('pago')
    	->with('registros', $registros);
    }

    public  function store(Request $request){


    	$referencia = $request->referencia;
    	$descripcion = $request->descripcion;
    	$moneda = $request->moneda;
    	$monto = $request->monto;

    	$placeToPay = new PlacetoPay([
    			'login' => '6dd490faf9cb87a9862245da41170ff2',
    			'tranKey' => '024h1IlD',
    			'url' => 'https://test.placetopay.com/redirection',
    	]);

		$request = [
		    'payment' => [
		        'reference' => $referencia,
		        'description' => $descripcion,
		        'amount' => [
		            'currency' => $moneda,
		            'total' => $monto,
		        ],
		    ],
		    'expiration' => date('c', strtotime('+2 days')),
		    'returnUrl' => 'http://localhost:8000/respuesta',
		    'ipAddress' => '127.0.0.1',
		    'userAgent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
		];

		$response = $placeToPay->request($request);
		if ($response->isSuccessful()) {
		    // STORE THE $response->requestId() and $response->processUrl() on your DB associated with the payment order
		    // Redirect the client to the processUrl or display it on the JS extension
		    
		    Session::put('requestId', $response->requestId);
		    Session::put('url', $response->processUrl);

		} else {
		    // There was some error so check the message and log it
		    $response->status()->message();
		}


    	return redirect($response->processUrl);
    }

    public function respuesta(){

    	$placeToPay = new PlacetoPay([
    			'login' => '6dd490faf9cb87a9862245da41170ff2',
    			'tranKey' => '024h1IlD',
    			'url' => 'https://test.placetopay.com/redirection',
    	]);
    	$respuesta = $placeToPay->query(Session::get('requestId'));

    	

    	if ($respuesta->isSuccessful()){

    		$registro = new Registro();
	    	$registro->requestId = $respuesta->requestId() ;
	    	$registro->status = $respuesta->status()->status() ;
	    	$registro->reason = $respuesta->status()->reason() ;;
	    	$registro->message = $respuesta->status()->message() ;
	    	$registro->date = $respuesta->status()->date() ;

	    	Cache::put('requestId',$registro->requestId, now()->addMinutes(10));
	    	Cache::put('message',$registro->message, now()->addMinutes(10));
	    	Cache::put('date',$registro->date, now()->addMinutes(10));
	    	Cache::put('status',$registro->status, now()->addMinutes(10));

	    	$registro->save();
    		
    	}

    	return (view('pago'));
    }
}
